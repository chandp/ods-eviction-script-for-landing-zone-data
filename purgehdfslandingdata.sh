#!/bin/bash

############################################################################################################
# Project           : generic script
#
# Program name      : purgehdfslandingdata.sh 
#
# Author            : chandp
#
# Date created      : 20170522
#
#
#
# Purpose           :  ARGUMENTS to pass to this script: (1) [No of days] - Mandatory
#								 (2) [hdfs dir path] - Mandatory
#                      To find and delete files which is more than n days from the landing area.
#                         
# script run command:                         
# bash purgehdfslandingdata.sh 30 /user/chandp/landing
#
#
############################################################################################################


##############################################
# Validations 
##############################################

	####################################################
	# validate mandatory parameters are being supplied 
	####################################################


	usage="Usage: purgehdfslandingdata.sh [No of days] [dirpath]"
	if [ ! "$1" ] || [ ! "$2" ] 
	then
	  echo $usage
	  exit 1
	fi

	#################################################
	# validate parameters ($1) is a integer
	#################################################

	re='^[0-9]+$'
	if ! [[ "$1" =~ $re ]] ; then
	   echo "Error occured: [No of days] is not a integer...==>" $1
	   exit 1
	fi

	#################################################
	# validate parameters ($2) for existance
	#################################################

	if $(hadoop fs -test -d "$2") ; then 
		echo "ok";
	else 
		echo "Error occured:Invalid hdfs directory path... ==>" $2 
		exit
	fi


if [ -f "landing_dir_path.log" ];	then
	rm landing_dir_path.log
	echo "file log_load.log removed"
else
	echo "file log_load.log doesnot exists"
fi 

#######################################################################
# SET UP LOG AND CHECK ARGUMENTS
#######################################################################
# get name of script
 SCRIPTNAME=$0
# get uuid for this run
 RUNID=$(uuidgen -r)

CURRENT_DATE_TIME=$(date +"%Y_%m_%d_%H_%M_%S")
LOG_LOCATION="purgelandinglog_${CURRENT_DATE_TIME}.txt"
TMPLOG_LOCATION="templog_${CURRENT_DATE_TIME}.txt"


#current system date
now=$(date +%s)


echo "Log location: ${LOG_LOCATION}"
echo "Log temp location: ${TMPLOG_LOCATION}"




log_message () {
    echo $(date --rfc-3339='seconds')'|'${SCRIPTNAME}'|'${RUNID}'|'$1
    echo $(date --rfc-3339='seconds')'|'${SCRIPTNAME}'|'${RUNID}'|'$1 >> ${LOG_LOCATION}
}

log_hive_message() {
    echo $(date --rfc-3339='seconds')'|'${SCRIPTNAME}'|'${RUNID}'|'$1
    echo $(date --rfc-3339='seconds')'|'${SCRIPTNAME}'|'${RUNID}'|'$1 
    cat hive-load.log >> ${LOG_LOCATION}
}


##########################################################################################
#read hdfs directory check the difference write it to the log_load.log file
##########################################################################################

log_message "------------Start of script-------------"

hadoop fs -lsr "$2" | grep "^d" | while read f; do 
  dir_date=`echo $f | awk '{print $6}'`
  difference=$(( ( $now - $(date -d "$dir_date" +%s) ) / (24 * 60 * 60 ) ))
  if [ $difference -gt $1 ]; then
     list=`echo $f | awk '{print $8}'`
     log_message "hdfs directory getting deleted ==>> $list" 	
     echo  $list >> landing_dir_path.log
    ##hadoop fs -rm -r $list
    ##hadoop fs -ls -R /user/chandp/landing 
    ##exit 1
    
    
  fi
   
done


filename="landing_dir_path.log"

if [ ! -f $filename ];then
	log_message "Info: There are no hdfs files found within the range give criteria  [ $1 no of days]"
fi 


##########################################################################################
#While loop to read line by line assign the values to the variable for futher processing
##########################################################################################
while read -r line
do
    readLine=$line
    #If the line starts with ST then echo the line
    if [[ $readLine = /user/*/landing/* ]] ; then
 
        table_name=`echo "$readLine" | cut -d/ -f 8`
        tdate=`echo "$readLine" | cut -d/ -f 7`
        date_part=`echo $tdate | sed 's/-/\_/g'`
        schema_name=`echo "$readLine" | cut -d/ -f 6`
        db_name=`echo "$readLine" | cut -d/ -f 5`
        
        if [ ! -z "$table_name" ]; then ##&& ["$schema_name" <>'user' ]]

		if [ "$schema_name" != "user" ] &&  [ "$table_name" != "landing" ] ; then
       	 
		dropsqlstr="drop table  lz_${db_name}_${schema_name}.${table_name}_${date_part};"
		log_message "------------ hive execution output message -------------"
 		log_message "hive table is getting dropped ==>  $dropsqlstr "
		# generate hive DROP table dynamically and dropping. 
		#hive -e "drop table  lz_${db_name}_${schema_name}.${table_name}_${date_part}"  > hive-load.log 2>&1 	

		
		log_hive_message " "
		log_message "------------ end of hive execution report -------------"

		fi
	
         fi 
        
      
    fi
done < "$filename"

log_message "------------End of script-------------"

